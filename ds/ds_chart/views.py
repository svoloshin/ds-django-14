
# Create your views here.

from rest_framework.response import Response
from ds_chart.models import PyPiInfo
from rest_framework.decorators import api_view
from ds_chart.models import UAInfo
from ds_chart.serializers import PyPiInfoSerializer

import datetime


@api_view(['GET'])
def pypi_info_list(request):

    if request.method == 'GET':
        # from the tail get 300 elements and show with step 3
        format_datetime = "%Y-%m-%dT%H:%M:%S.%fZ"
        request_datetime_from = request.GET['datetime_from']
        request_datetime_to = request.GET['datetime_to']

        datetime_from = datetime.datetime.strptime(
            request_datetime_from,
            format_datetime
        )
        datetime_to = datetime.datetime.strptime(
            request_datetime_to,
            format_datetime
        )

        pypi_info = PyPiInfo.objects.filter(
            created_at__gte=datetime_from,
            created_at__lte=datetime_to).order_by('-id')[:50]

        serializer = PyPiInfoSerializer(pypi_info, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def user_agent_info(request):

    if request.method == 'GET':

        info = request.META['HTTP_USER_AGENT']

        ua_info = UAInfo.objects.create(ua_string=info)
        ua_info.save()

        return Response(info)
