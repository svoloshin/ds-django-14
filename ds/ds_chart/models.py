from django.db import models

# Create your models here.


class PyPiInfo(models.Model):
    projects = models.PositiveIntegerField()
    releases = models.PositiveIntegerField()
    files = models.PositiveIntegerField()
    users = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class UAInfo(models.Model):
    ua_string = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
