from django.apps import AppConfig


class DsChartConfig(AppConfig):
    name = 'ds_chart'
