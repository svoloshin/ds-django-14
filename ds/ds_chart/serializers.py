from rest_framework import serializers

from ds_chart.models import PyPiInfo
from ds_chart.models import UAInfo

class PyPiInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PyPiInfo
        fields = ['projects', 'releases', 'files', 'users', 'created_at', 'updated_at']

class UAInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = UAInfo
        fields = ['ua_string', 'created_at', 'updated_at']