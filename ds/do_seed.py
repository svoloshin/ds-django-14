#!/usr/bin/python
import sys
import os
import django

sys.path.append('ds')
# os.environ['DJANGO_SETTINGS_MODULE']
django.setup()

from django_seed import Seed

from ds_chart.models import PyPiInfo
from django.contrib.auth.models import User

import random

seeder = Seed.seeder()

seeder.add_entity(PyPiInfo, 1000)
inserted_pks = seeder.execute()